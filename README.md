![Laravel Logo](img/laravel.png "Laravel Logo")

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel SPP PAUD
Ini adalah contoh aplikasi menggunakan laravel dengan studi kasus Sistem Informasi Pembayaran SPP dan Tabungan untuk Paud 

### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-spp-paud.git`
2. Go inside the folder: `cd laravel-spp-paud`
3. Install dependencies by Running this command: `composer install`
4. Create database for this app then copy the example env file and make the required database configuration changes in the .env file: `cp .env.example .env`
5. Run the database migrations (Set the database connection in .env before migrating): `php artisan migrate --seed`
6. Start the local development server: `php artisan serve`
7. Open your favorite browser. You can now access the server at http://localhost:8000

### Default SuperUser

Default Email : demo link

email: admin@example.com 

Password : password


### Image Screen shot

Login Page

![Login Page](img/login.png "Login Page")

Dashboard Page

![Dashboard Page](img/dashboard.png "Dashboard Page")

User Profile

![User Profile](img/user-profile.png "User Profile")

Add User

![Add User](img/add-user.png "Add User")

List Users

![List Users](img/list-users.png "List Users")

Edit Users

![Edit Users](img/edit-user.png "Edit Users")

Pengaturan Page

![Pengaturan Page](img/pengaturan.png "Pengaturan Page")

Buku Panduan

![Buku Panduan](img/buku-panduan.png "Buku Panduan")


Periode Page

![Periode Page](img/add-periode.png "Periode Page")

Delete Periode

![Delete Periode](img/delete-periode.png "Delete Periode")

Kelas Page

![Kelas Page](img/add-kelas.png "Kelas Page")

Edit Kelas

![Edit Kelas](img/edit-kelas.png "Edit Kelas")

Siswa Kelas

![Siswa Kelas](img/add-siswa.png "Siswa Kelas")

Edit Kelas

![Edit Kelas](img/edit-kelas.png "Edit Kelas")

Details Siswa

![Details Siswa](img/details-siswa.png "Details Siswa")

Tagihan Page

![Tagihan Page](img/list-tagihan.png "Tagihan Page")

Edit Tagihan

![Edit Tagihan](img/edit-tagihan.png "Edit Tagihan")

Keuangan Page

![Keuangan Page](img/list-keuangan.png "Keuangan Page")

Tabungan Page

![Tabungan Page](img/list-tabungan.png "Tabungan Page")

SPP Page

![SPP Page](img/list-spp.png "SPP Page")



