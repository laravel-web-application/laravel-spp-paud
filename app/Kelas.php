<?php

namespace App;

use App\Http\Controllers\SiswaController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kelas extends Model
{
    use SoftDeletes;

    protected $table = 'kelas';

    protected $fillable = [
        'periode_id',
        'nama'
    ];

    public function siswa()
    {
        return $this->hasMany(SiswaController::class, 'kelas_id', 'id');
    }

    public function periode()
    {
        return $this->hasOne(Periode::class, 'id', 'periode_id');
    }
}
