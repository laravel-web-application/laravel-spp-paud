<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tabungan extends Model
{
    use SoftDeletes;

    protected $table = 'tabungan';

    protected $fillable = [
        'siswa_id',
        'tipe',
        'jumlah',
        'saldo',
        'keperluan'
    ];

    public function siswa()
    {
        return $this->belongsTo('App\Siswa', 'siswa_id', 'id');
    }

    public function keuangan()
    {
        return $this->hasOne('App\Keuangan', 'tabungan_id', 'id');
    }
}
