<?php

namespace App\Exports;

use App\Keuangan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class KeuanganExport implements FromView
{
    public function view(): View
    {
        return view('keuangan.export', [
            'keuangan' => $this->collection()
        ]);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Keuangan::all();
    }
}

