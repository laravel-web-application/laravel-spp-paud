<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Siswa extends Model
{
    use SoftDeletes;

    protected $table = 'siswa';

    protected $fillable = [
        'kelas_id',
        'nama',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'alamat',
        'nama_wali',
        'telp_wali',
        'pekerjaan_wali',
        'is_yatim'
    ];

    public function kelas()
    {
        return $this->hasOne('App\Kelas', 'id', 'kelas_id');
    }

    public function transaksi()
    {
        return $this->hasMany('App\Transaksi', 'siswa_id', 'id');
    }

    public function role()
    {
        return $this->hasMany('App\Role', 'siswa_id', 'id');
    }

    public function tabungan()
    {
        return $this->hasMany('App\Tabungan', 'siswa_id', 'id');
    }
}
