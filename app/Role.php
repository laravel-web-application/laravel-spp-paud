<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $table = 'roles';

    protected $fillable = [
        'tagihan_id',
        'siswa_id'
    ];

    public function siswa()
    {
        return $this->hasOne('App\Siswa', 'id', 'siswa_id');
    }

    public function tagihan()
    {
        return $this->hasOne('App\Tagihan', 'id', 'tagihan_id');
    }
}
