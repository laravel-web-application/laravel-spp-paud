<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
            'role' => 'Admin',
            'created_at' => now()
        ],
            [
                'name' => 'Uzumaki Naruto',
                'email' => 'uzumaki_naruto@konohagakure.com',
                'password' => bcrypt('naruto2020'),
                'role' => 'Super Admin',
                'created_at' => now()
            ]);
    }
}
